import React, {useState, useEffect} from 'react';
import moment from "moment";
import {makeStyles} from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';

import SearchMovies from "./components/searchMovies/searchMovies";
import CardList from "./components/cardList/CardList";

const { REACT_APP_API_URL, REACT_APP_API_LOCALIZATION, REACT_APP_API_KEY } = process.env
const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    flexGrow: 1,
    display: 'none',
    [theme.breakpoints.up('sm')]: {
      display: 'block',
    },
  },
  list: {
    width: 250,
  },
  fullList: {
    width: 'auto',
  },
}));

export default function App() {
  const classes = useStyles();
  const [moviesList, setMovies] = useState([])
  const [listTitle, setListTitle] = useState("Today's Popular Movies")
  const [state, setState] = React.useState({left: false});
  const [isFavoritesOnly, showingFavoritesList] = useState(false)
  const toggleDrawer = (anchor, open) => (event) => {
    if (event.type === 'keydown' && (event.key === 'Tab' || event.key === 'Shift')) {
      return;
    }
    setState({ ...state, [anchor]: open });
  };

  useEffect(() => {
    let curatedMovieList = localStorage.getItem('curated')
    if (curatedMovieList) {
        curatedMovieList = JSON.parse(curatedMovieList)
    }else {
      curatedMovieList = {
        title: 'My Favorite Movies',
        movies: []
      }
    }
    localStorage.setItem('curated', JSON.stringify(curatedMovieList))
  }, [])

  useEffect(() => {
    const apiUrl = `${REACT_APP_API_URL}movie/popular?api_key=${REACT_APP_API_KEY}&language=${REACT_APP_API_LOCALIZATION}&page=1`
    fetch(apiUrl).then(resp => resp.json()).then(data => {
       updateMovieList(data.results)
     })
    },[])

  const updateMovieList = function(movies) {
    const moviesList = movies.filter(movie => movie.poster_path)
    moviesList.map(movie => {
      movie.release_date = moment(movie.release_date).format("MMMM Do, YYYY")
      return movie
    })
    setMovies(moviesList)
  }
  const updateListTitle = function(title){
    setListTitle(title)
  }
  const changeMovieList = async function(listSlug) {
    let listTitle = ''
    switch(listSlug){
      case 'now_playing':
        listTitle = 'Now Playing'
        showingFavoritesList(false)
        break;
      case 'top_rated':
        listTitle = 'Top Rated Movies'
        showingFavoritesList(false)
        break;
      case 'popular':
        listTitle = "Today's Popular Movies"
        showingFavoritesList(false)
        break;
      case 'curated':
        listTitle = "My Favorite Movies"
        showingFavoritesList(true)
        break;
      default:
        listTitle = ''
    }
    updateListTitle(listTitle)
    if(listSlug === 'curated') {
      let moviesList = JSON.parse(localStorage.getItem('curated'))
      moviesList = moviesList.movies
      setMovies(moviesList)
    }else{
      const apiUrl = `${REACT_APP_API_URL}movie/${listSlug}?api_key=${REACT_APP_API_KEY}&language=${REACT_APP_API_LOCALIZATION}&page=1`
      const response = await fetch(apiUrl)
      const data = await response.json()
      const moviesList = data.results
      updateMovieList(moviesList)
    }
  }
  return (
    <div className="App">
      <div className="container">
        <AppBar position="static">
          <Toolbar>
            <IconButton edge="start" className={classes.menuButton} color="inherit" aria-label="open drawer" onClick={toggleDrawer('left', true)}>
              <MenuIcon />
            </IconButton>
            <Typography className={classes.title} variant="h6" noWrap>
              IM(not)DB | "It's like IMDB, but not."
            </Typography>
            <SearchMovies updateMovieList={updateMovieList} updateListTitle={updateListTitle} />
          </Toolbar>
        </AppBar>
        <CardList moviesList={moviesList} listTitle={listTitle} isFavoritesOnly={isFavoritesOnly} />
        <Drawer anchor='left' open={state['left']} onClose={toggleDrawer('left', false)}>
          <div className={classes.list} role="presentation" onClick={toggleDrawer('left', false)} onKeyDown={toggleDrawer('left', false)}>
            <List>
              <ListItem button key='Popular' onClick={() => changeMovieList('popular')}>
                <ListItemText primary='Popular' />
              </ListItem>
              <ListItem button key='Top Rated' onClick={() => changeMovieList('top_rated')}>
                <ListItemText primary='Top Rated' />
              </ListItem>
              <ListItem button key='Now Playing' onClick={() => changeMovieList('now_playing')}>
                <ListItemText primary='Now Playing' />
              </ListItem>
              <ListItem button key='Favorites List' onClick={() => changeMovieList('curated')}>
                <ListItemText primary='My Favorited Movies' />
              </ListItem>
            </List>
          </div>
        </Drawer>
      </div>
    </div>
  );
}