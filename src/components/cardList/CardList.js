import React, { useEffect } from "react";
import Dialog from "@material-ui/core/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import Slide from '@material-ui/core/Slide';
import Typography from "@material-ui/core/Typography";
import MovieCard from "../movieCard/MovieCard";
import './CardList.css'

const { REACT_APP_API_URL, REACT_APP_API_KEY, REACT_APP_API_LOCALIZATION } = process.env

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

export default function CardList({ moviesList = [], listTitle = 'Default List Title', isFavoritesOnly = false }){
  const [movieDetail, setMovieDetail] = React.useState({});
  const [isDialogOpen, setOpen] = React.useState(false);
  const [, updateState] = React.useState();
  const forceUpdate = React.useCallback(() => updateState({}), []);

  useEffect(() => {
    if(Object.entries(movieDetail).length === 0) return
    setOpen(true)
  }, [movieDetail])

  useEffect(() => {
    moviesList.map(movie => {
      let curatedMovieList = localStorage.getItem('curated')
      if (curatedMovieList) {
        curatedMovieList = JSON.parse(curatedMovieList)
        if (curatedMovieList?.movies?.length > 0) {
          let index = curatedMovieList.movies.findIndex(favoritedMovie => favoritedMovie.id === movie.id)
          movie.is_favorite = index >= 0
        }
      }
      return movie
    })
  }, [moviesList])

  const handleClose = function() {
    setOpen(false);
  };

  const updateFavorites = function(selectedMovie) {
    console.log("selectedMovie", selectedMovie)
    let curatedMovieList = localStorage.getItem('curated')
    curatedMovieList = JSON.parse(curatedMovieList)
    if (curatedMovieList?.movies?.length > 0) {
      let index = curatedMovieList.movies.findIndex(favoritedMovie => favoritedMovie.id === selectedMovie.id)
      if(index >= 0){
        curatedMovieList.movies.splice(index, 1)
        if(isFavoritesOnly) {
          let favoriteIndex = moviesList.findIndex(movie => movie.id === selectedMovie.id)
          console.log("favoriteIndex", favoriteIndex)
          console.log("isFavoriteOnly", isFavoritesOnly)
          if(favoriteIndex >= 0) {
            moviesList.splice(favoriteIndex, 1)
          }
        }
      }else
        curatedMovieList.movies.push(selectedMovie)
    }else {
      curatedMovieList.movies.push(selectedMovie)
    }
    forceUpdate()
    localStorage.setItem('curated', JSON.stringify(curatedMovieList))
  }

  const openMovieDetailDialog = function(movieId) {
    getMovieDetails(movieId).then((data) => {
      setMovieDetail(updateMovieDetail(data))
    })
  }

  const getMovieDetails = async function(movieId, openDialog = false){
    const apiUrl = `${REACT_APP_API_URL}movie/${movieId}?api_key=${REACT_APP_API_KEY}&language=${REACT_APP_API_LOCALIZATION}`

    try {
      const apiResponse = await fetch(apiUrl);
      return await apiResponse.json()
    }catch(apiError){
      console.log(apiError)
    }
  }

  const updateMovieDetail = function(data) {
    const movieDetail = data
    let genres = data.genres.map(genre => genre.name)
    genres = genres.join(', ')
    movieDetail.genres = genres
    return movieDetail
  }

  return (
    <>
      <Typography title="true" className="card-list-title" variant="h3">
        {listTitle}
      </Typography>
      <div className="card-list">
        {moviesList.map(movie => (
          <MovieCard movie={movie} key={movie.id} is_favorite={movie.is_favorite} openDialog={openMovieDetailDialog} getMovieDetails={getMovieDetails} updateFavorite={updateFavorites} />
        ))}
      </div>
      <Dialog
        open={isDialogOpen}
        TransitionComponent={Transition}
        keepMounted
        onClose={handleClose}
        aria-labelledby="alert-dialog-slide-title"
        aria-describedby="alert-dialog-slide-description">
        <DialogTitle id="alert-dialog-slide-title">{movieDetail.title}</DialogTitle>
        <DialogContent>
          <Typography paragraph>
            <strong>Genre(s):</strong> {movieDetail.genres}
          </Typography>
          <Typography paragraph>
            <strong>Rating:</strong> {movieDetail.vote_average}/10 ({movieDetail.vote_count} votes)
          </Typography>
          <Typography paragraph>
            <strong>Overview:</strong> {movieDetail.overview}
          </Typography>
        </DialogContent>
      </Dialog>
    </>
  )
}