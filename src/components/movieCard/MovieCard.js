import React, {useEffect, useState} from 'react';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardMedia from '@material-ui/core/CardMedia';
import CardActions from '@material-ui/core/CardActions';
import IconButton from '@material-ui/core/IconButton';
import Button from "@material-ui/core/Button";
import FavoriteIcon from '@material-ui/icons/Favorite';

import './MovieCard.css'

export default function MovieCard(props) {
  const [isFavorite, setFavorite] = useState(false)

  const updateFavoriteState = function(movie) {
    setFavorite(!movie.is_favorite)
    movie.is_favorite = !movie.is_favorite
    props.updateFavorite(movie)
  }

  useEffect(() => {
    setFavorite(props.movie.is_favorite)
  },[props.movie.is_favorite])
  return(
    <>
      <Card className="card" variant="outlined">
        <CardHeader
          className="card-title"
          title={props.movie.title}
          subheader={"Released " + props.movie.release_date}
        />
        <CardMedia className="card-image" component="img" image={`https://image.tmdb.org/t/p/w185_and_h278_bestv2${props.movie.poster_path}`} alt={'Contains a poster image for ' + props.movie.title} />
        <CardActions>
          <IconButton aria-label="add to favorites" onClick={() => updateFavoriteState(props.movie)}>
            <FavoriteIcon color={isFavorite ? "secondary" : "disabled"} />
          </IconButton>
          <Button className="more-detail" onClick={() => props.openDialog(props.movie.id)}>More Info</Button>
        </CardActions>
      </Card>
    </>
  )
}