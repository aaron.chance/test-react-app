import React, { useState } from "react";
import InputBase from '@material-ui/core/InputBase';
import { fade, makeStyles } from '@material-ui/core/styles';
import SearchIcon from '@material-ui/icons/Search';

import "./searchMovies.css"

const { REACT_APP_API_URL, REACT_APP_API_KEY, REACT_APP_API_LOCALIZATION } = process.env

const useStyles = makeStyles((theme) => ({
  search: {
    position: 'relative',
    borderRadius: theme.shape.borderRadius,
    backgroundColor: fade(theme.palette.common.white, 0.15),
    '&:hover': {
      backgroundColor: fade(theme.palette.common.white, 0.25),
    },
    marginLeft: 0,
    width: '100%',
    [theme.breakpoints.up('sm')]: {
      marginLeft: theme.spacing(1),
      width: 'auto',
    },
  },
  searchIcon: {
    padding: theme.spacing(0, 2),
    height: '100%',
    position: 'absolute',
    pointerEvents: 'none',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  inputRoot: {
    color: 'inherit',
  },
  inputInput: {
    padding: theme.spacing(1, 1, 1, 0),
    // vertical padding + font size from searchIcon
    paddingLeft: `calc(1em + ${theme.spacing(4)}px)`,
    transition: theme.transitions.create('width'),
    width: '100%',
    [theme.breakpoints.up('sm')]: {
      width: '20ch',
      '&:focus': {
        width: '35ch',
      },
    },
  },
}));

export default function SearchMovies(props) {
  const classes = useStyles();
  const [movieQuery, setQuery] = useState('')

  const updateMovieQuery = function(event) {
    setQuery(event.target.value)
  }
  const searchMovies = async function(event) {
    event.preventDefault()
    const apiUrl = `${REACT_APP_API_URL}search/movie?api_key=${REACT_APP_API_KEY}&language=${REACT_APP_API_LOCALIZATION}&query=${movieQuery}&page=1`

    try {
      const apiResponse = await fetch(apiUrl);
      const data = await apiResponse.json()
      if(data.results.length > 0) {
        props.updateMovieList(data.results)
        props.updateListTitle(`Search Results for "${movieQuery}"`)
      }else {
        props.updateMovieList([])
        props.updateListTitle(`No Results Found for "${movieQuery}"`)
      }
    }catch(apiError){
      console.log(apiError)
    }
  }

  return (
    <form className={classes.search} onSubmit={searchMovies}>
      <div className={classes.searchIcon}>
        <SearchIcon />
      </div>
      <InputBase
        placeholder="Search…"
        value={movieQuery} onChange={updateMovieQuery}
        classes={{
          root: classes.inputRoot,
          input: classes.inputInput,
        }}
        inputProps={{ 'aria-label': 'search' }}
      />
    </form>
  )
}