This project was written of how I'd build an application with React that demonstrates use of state, remote resources (APIs, static data, etc), local storage, and using a modular style.

## Requirements

The app connects to [The Movie Database](https://www.themoviedb.org/documentation/api) api (keys provided for the purposes of demo -- credentials not ordinarily stored in plain text, or inside a repository)

The application enables a user to:
- See Popular, Top Rated, or Now Playing movies
- Allow a user to search for a movie
- See details about a selected movie
- Save a personal list of favorite movies

## Instructions to run:
- Clone the repo
In the project directory, you can run:
- run `npm install` to install project's dependencies
- once those finish installing, run `npm start` to run the app in development mode
- The development server will open [http://localhost:3000](http://localhost:3000) automatically to view it in the browser, once compilation completes.